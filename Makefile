env:
	pip3 install virtualenv
	virtualenv -p python3.7 venv
	source venv/bin/activate
	echo '-- Environment is ready'

run:
	FLASK_APP='service/app.py' flask run
    
