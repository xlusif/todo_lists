## Todo rest api


### Documentation for initializing the runtime environment
Described in `Makefile`. Command: `make env`
### Documentation for starting the service
Described in `Dockerfile`
### Documentation for required configuration
`service/config.py` file. Contains mongo connection parameters
### JSON requests and responses
POST `http://0.0.0.0:5000/api/v1/todo`  
Request:  
```
{
    "name": "New todo"
}
```
Response:  
status: 201
```
{
    "_id": "5e302d23fa067f707c957c30"
}
```
---
POST `http://0.0.0.0:5000/api/v1/todo/5e302d23fa067f707c957c30`  
Request:  
```
{
    "text": "Todo item text",
    "due_date": "28.01.2020",
    "finished_status": "In progress"
}
```
Response:  
status: 201
```
{
    "_id": "5e302e07fa067f707c957c31"
}
```
---
GET `http://0.0.0.0:5000/api/v1/todo/5e302d23fa067f707c957c30` 
 
Request:
Response:  
status: 200
```
{
    "_id": "5e302d23fa067f707c957c30",
    "name": "New todo",
    "todo_items": [
        {
            "text": "Todo item text",
            "due_date": "28.01.2020",
            "finished_status": "In progress",
            "_id": "5e302e07fa067f707c957c31"
        }
    ]
}
```
---
GET `http://0.0.0.0:5000/api/v1/todo`  
Request:  
Response:  
status: 200
```
[
    {
        "_id": "5e302d23fa067f707c957c30",
        "name": "New todo",
        "todo_items": [
            {
                "text": "Todo item text",
                "due_date": "28.01.2020",
                "finished_status": "In progress",
                "_id": "5e302e07fa067f707c957c31"
            }
        ]
    }
]
```
---
PUT `http://0.0.0.0:5000/api/v1/todo/5e302d23fa067f707c957c30`  
Request:
```
{
    "name": "Renamed todo"
}
```
Response:  
status: 200
```
{
    "message": "Todo is updated"
}
```
---
DELETE `http://0.0.0.0:5000/api/v1/todo/5e302d23fa067f707c957c30/5e302e07fa067f707c957c31`  
Request:  
Response:  
status: 200
```
{
    "message": "Todo item is deleted"
}
```
---
DELETE `http://0.0.0.0:5000/api/v1/todo/5e302d23fa067f707c957c30`  
Request:  
Response:  
status: 200
```
{
    "message": "Todo is deleted"
}
```