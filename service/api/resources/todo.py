from flask_restful import Resource, reqparse, abort

import service.database as db

todo_parser = reqparse.RequestParser()
todo_parser.add_argument('name', type=str)

todo_item_parser = reqparse.RequestParser()
todo_item_parser.add_argument('text', type=str)
todo_item_parser.add_argument('due_date', type=str)
todo_item_parser.add_argument('finished_status', type=str)


class TodoResource(Resource):
    """
    Resource for todo entity
    """

    def get(self, todo_id=None):
        if todo_id:
            todo = db.fetch_todo(todo_id)
            if todo is None:
                abort(404, message='Not found')
            return self.serialize_for_response(todo)

        todos = db.fetch_all_todos()
        return [self.serialize_for_response(todo) for todo in todos]

    def post(self, todo_id=None):
        if todo_id:
            args = todo_item_parser.parse_args()
            new_id = db.create_todo_item(todo_id, args)
            return {'_id': str(new_id)}, 201

        args = todo_parser.parse_args()
        new_id = db.create_todo(args)
        return {'_id': str(new_id)}, 201

    def put(self, todo_id, todo_item_id=None):
        if todo_item_id:
            args = todo_item_parser.parse_args()
            db.update_todo_item(todo_id, todo_item_id, args)
            return {'message': 'Todo item is updated'}

        args = todo_parser.parse_args()
        db.update_todo(todo_id, args)
        return {'message': 'Todo is updated'}

    def delete(self, todo_id, todo_item_id=None):
        if todo_item_id:
            db.delete_todo_item(todo_id, todo_item_id)
            return {'message': 'Todo item is deleted'}

        db.delete_todo(todo_id)
        return {'message': 'Todo is deleted'}

    @staticmethod
    def serialize_for_response(todo):
        todo['_id'] = str(todo['_id'])
        for todo_item in todo['todo_items']:
            todo_item['_id'] = str(todo_item['_id'])
        return todo
