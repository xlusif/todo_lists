from flask import Blueprint
from flask_restful import Api

from service.api.resources.todo import TodoResource


todo_bp = Blueprint('todo_api', __name__, url_prefix='/api/v1')

api = Api(todo_bp)

api.add_resource(TodoResource, '/todo',
                               '/todo/<string:todo_id>',
                               '/todo/<string:todo_id>/<string:todo_item_id>')
