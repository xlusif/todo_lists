from flask import Flask

from service.api.routes import todo_bp

app = Flask(__name__)

app.register_blueprint(todo_bp)
