import os


class Config:
    MONGO_USERNAME = os.environ.get('MONGO_USERNAME')
    MONGO_PASSWORD = os.environ.get('MONGO_USERNAME')
    MONGO_URL = f'mongodb://{MONGO_USERNAME}:{MONGO_PASSWORD}@mongo:27017'
