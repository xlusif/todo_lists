from bson import ObjectId
from pymongo import MongoClient
from service.config import Config

mongo_client = MongoClient(Config.MONGO_URL)
db = mongo_client.db


# todos
def fetch_todo(todo_id: str):
    return db.todo.find_one({'_id': ObjectId(todo_id)})


def fetch_all_todos():
    return db.todo.find()


def create_todo(data: dict):
    data['todo_items'] = []
    result = db.todo.insert_one(data)
    return result.inserted_id


def update_todo(todo_id: str, data: dict):
    db.todo.find_one_and_update({'_id': ObjectId(todo_id)}, {'$set': data})


def delete_todo(todo_id: str):
    db.todo.delete_one({'_id': ObjectId(todo_id)})


# todo_items
def create_todo_item(todo_id: str, data: dict):
    data['_id'] = ObjectId()
    db.todo.find_one_and_update({'_id': ObjectId(todo_id)},
                                {'$push': {'todo_items': data}})
    return data['_id']


def update_todo_item(todo_id: str, todo_item_id: str, data: dict):
    db.todo.find_one_and_update({'_id': ObjectId(todo_id), 'todo_items._id': ObjectId(todo_item_id)},
                                {'$set': data})


def delete_todo_item(todo_id: str, todo_item_id: str):
    db.todo.find_one_and_update({'_id': ObjectId(todo_id)},
                                {'$pull': {'todo_items': {'_id': ObjectId(todo_item_id)}}})
